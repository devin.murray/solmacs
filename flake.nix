{
  description = "Devin's Emacs Configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.eachDefaultSystem (system: let
    pkgs = import nixpkgs {
      inherit system;
    };
  in {
    devShell = pkgs.mkShell {
      buildInputs = [
        pkgs.emacs29-pgtk
        pkgs.ripgrep
        pkgs.nodejs
        pkgs.nil
      ];
    };
  });
}
