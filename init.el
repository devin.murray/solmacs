;;
;; Early Configurations
;;

(defun join-path (path filename)
  "Concat path and file. Add '/' to the end of the path if necessary"
  (concat path (if (string-match-p "/$" path) "" "/") filename))

;; Define config directories
(defconst solmacs-d-dir (file-name-directory user-init-file))
(defconst solisp-dir (join-path solmacs-d-dir "solisp"))
(defconst solmacs-config-dir (join-path solmacs-d-dir "config"))

;; Install use-package to load packages in with straight.el
(straight-use-package 'use-package)

(use-package straight
  :custom (straight-use-package-by-default 1))
(setq use-package-always-ensure t)

;; Setup proper PATH for MacOS
(when (equal system-type 'darwin)
  (use-package exec-path-from-shell
    :init
    (exec-path-from-shell-initialize)))

;;
;; Cleanup
;;

;; Store files in cache
(setq user-emacs-directory (expand-file-name "~/.cache/emacs/")
      url-history-file (expand-file-name "url/history" user-emacs-directory))

;; Install package for cleanup
(use-package no-littering)

;; Keep customizations in a temp file
(setq custom-file
(if (boundp 'server-socket-dir)
    (expand-file-name "custom.el" server-socket-dir)
  (expand-file-name (format "emacs-custom-%s.el" (user-uid)) temporary-file-directory)))

;;
;; Load modules
;;

;; Add dirs to load-path
(dolist (dir (list solisp-dir))
  (let ((default-directory dir))
    (normal-top-level-add-subdirs-to-load-path)))

;; Load configurations
(let ((config-files (directory-files
                     solmacs-config-dir
                     t (rx (and (+ (| alphanumeric "." "-"))
                                ".el"
                                line-end)))))
  (mapcar 'load-file config-files))

;;
;; Emacs configurations
;

;; Use unicode for all the things
(set-default-coding-systems 'utf-8)     ; Default to utf-8 encoding
(prefer-coding-system       'utf-8)     ; Add utf-8 at the front for automatic detection.
(set-default-coding-systems 'utf-8)     ; Set default value of various coding systems
(set-terminal-coding-system 'utf-8)     ; Set coding system of terminal output
(set-keyboard-coding-system 'utf-8)     ; Set coding system for keyboard input on TERMINAL
(set-language-environment "English")    ; Set up multilingual environment

;; Change Scratch mode
(setq initial-scratch-message nil
      initial-major-mode 'org-mode)

(customize-set-variable 'visible-bell 1)
(customize-set-variable 'large-file-warning-threshold 100000000)

;; Disable file lockin
(setq create-lockfiles nil)
