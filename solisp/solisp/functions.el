(defun read-lines (file n)
  "Return the N lines from the file"
  (with-temp-buffer
    (insert-file-contents-literally file)
    (cl-loop repeat n
             unless (eobp)
             collect (prog1 (buffer-substring-no-properties
                             (line-beginning-position)
                             (line-end-position))
                       (forward-line 1)))))

;; Function to detect ansible files
;; (defun detect-ansible ()
;;   (when (and buffer-file-name
;;              (string= (file-name-extension buffer-file-name) "yml")
;;              (string = (file-name-extension (buffer-file-name) t) "yaml")
;;              (save-excursion
;;                (goto-char (point-min))
;;                (looking-at "---")))
;;     'ansible))

;; Function to launch lsp
;; (defun my-yaml-mode-hook ()
;;   "Hook for detecting Ansible YAML files."
;;   (when (and (string= (file-name-extension buffer-file-name) "yml")
;;              (string-match-p "^---" (buffer-string)))
;;     (ansible 1)
;;     (lsp-deferred)))

;; (add-hook 'yaml-mode-hook #'my-yaml-mode-hook)
