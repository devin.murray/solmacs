;;
;; Garbage Collection
;;

(setq gc-cons-threshold (* 32 1024 1024)) ;; The initial threshold

;; Increase threshold on startup
(add-hook 'emacs-startup-hook
	  (lambda () (setq gc-cons-threshold (* 128 1024 1024))))

;;
;; Native Compiltation
;;

;; Load the newest compiled .el file
(customize-set-variable 'load-prefer-newer noninteractive)

;; Change native compile settings
(when (featurep 'native-compile)
  (setq native-comp-async-report-warnings-errors nil) ;; Silence warnings
  (setq native-comp-deferred-complitation t) ;; Async compiling

  ;; Set the elisp compile cache directory depending on version of emacs
  (when (fboundp 'startup-redirect-eln-cache)
    (if (version< emacs-version "29")
	      (add-to-list 'native-comp-eln-load-path (convert-standard-filename (expand-file-name "var/eln-cache/" user-emacs-directory)))
      (startup-redirect-eln-cache (convert-standard-filename (expand-file-name "var/eln-cache/" user-emacs-directory)))))
  (add-to-list 'native-comp-eln-load-path (expand-file-name "eln-cache/" user-emacs-directory)))

;;
;; Package Manager straight.el
;;

(setq straight-check-for-modifications nil) ;; for init speed up

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)

;;
;; User interface
;;

;; Disable UI elements
(setq inhibit-startup-messages t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 10)

;; Only use menubar on macOS
(unless (equal system-type 'darwin)
  (menu-bar-mode -1))

;; QOL
;;

;; Load dashboard faster by making it fundamental-mode
(customize-set-variable 'initial-major-mode 'fundamental-mode)

;; Alias yes and no
(defalias 'yes-or-no-p 'y-or-n-p)

;; Set tabbing preferences
(setq-default tab-width 2
	      indent-tabs-mode nil)
