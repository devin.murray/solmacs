;; Enable LSP
(use-package lsp-mode
  :init
  (setq lsp-keymap-prefix "C-c l")
  :hook (
         (autotools-mode . lsp-deferred)
         (awk-mode . lsp-deferred)
         (sh-mode . lsp-deferred)
         (c-mode . lsp-deferred)
         (c++-mode . lsp-deferred)
         (csharp-mode . lsp-deferred)
         (dockerfile-mode . lsp-deferred)
         (go-mode . lsp-deferred)
         (html-mode . lsp-deferred)
         (java-mode . lsp-deferred)
         (javascript-mode . lsp-deferred)
         (json-mode . lsp-deferred)
         (kotlin-mode . lsp-deferred)
         (lua-mode . lsp-deferred)
         (markdown-mode . lsp-deferred)
         (nix-mode . lsp-deferred)
         (php-mode . lsp-deferred)
         (powershell-mode . lsp-deferred)
         (python-mode . lsp-deferred)
         (ruby-mode . lsp-deferred)
         (sql-mode . lsp-deferred)
         (terraform-mode . lsp-deferred)
         (typescript-mode . lsp-deferred)
         (xml-mode . lsp-deferred)
         (yaml-mode . lsp-deferred)
         (lsp-mode . lsp-enable-which-key-integration))
  :config
  (setq lsp-headerline-breadcrumb-enable nil
        lsp-ui-doc-enable t
        lsp-ui-doc-use-childframe t
        lsp-ui-doc-position 'top
        lsp-ui-doc-include-signature t
        lsp-ui-sideline-enable nil
        lsp-ui-sideline-show-code-actions nil
        lsp-ui-sideline-show-diagnostics nil
        lsp-ui-flycheck-enable t
        lsp-ui-flycheck-list-position 'right
        lsp-ui-flycheck-live-reporting t
        lsp-ui-peek-enable t
        lsp-ui-peek-list-width 60
        lsp-ui-peek-peek-height 25
        lsp-semantic-tokens-enable t
        lsp-semantic-tokens-honor-refresh-requests t)
       (setq lsp-disabled-clients '(tfls))
  :commands lsp)
;; LSP Extensions
(use-package lsp-ui
  :requires lsp-mode flycheck
  :hook ( lsp-mode )
  :commands lsp-ui-mode)

(use-package lsp-treemacs
  :commands lsp-treemacs-error-list)

(use-package flycheck
  :ensure t
  :config
  (global-flycheck-mode))

(use-package dap-mode)
