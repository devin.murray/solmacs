(use-package super-save
  :ensure t
  :defer 1
  :diminish super-save-mode
  :config
  (super-save-mode +1)
  (setq super-save-auto-save-when-idle t))

;; Auto revert files
(global-auto-revert-mode 1)

;; Auto pair brackets
(electric-pair-mode 1)
(setq electric-pair-preserve-balance t
      electric-pair-delete-adjacent-pairs t
      electric-pair-open-newline-between-pairs nil)

 ;; Highlight matching braces
(use-package paren
  :config
  (set-face-attribute 'show-paren-match-expression nil :background "#363e4a")
  (show-paren-mode 1))

 ;; Remove Whitespace
(use-package ws-butler
  :hook (
         (text-mode . ws-butler-mode)
         (prog-mode . ws-butler-mode)))

(use-package highlight-indent-guides
  :hook (( prog-mode yaml-mode json-mode) . highlight-indent-guides-mode)
  :config
  (setq highlight-indent-guides-method 'character))

;; Undo tree

(use-package undo-fu)

(use-package undo-fu-session
  :config
  (setq undo-fu-session-incompatible-files '("/COMMIT_EDITMSG\\'" "/git-rebase-todo\\'"))
  (global-undo-fu-session-mode))

(use-package vundo)

(use-package consult
  :hook (completion-list-mode . consult-preview-at-point-mode))
