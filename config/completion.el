;;
;; Completion Framework
;;
(use-package vertico
    :straight (:files (:defaults "extensions/*")
                     :includes (vertico-indexed
                                vertico-flat
                                vertico-grid
                                vertico-mouse
                                vertico-quick
                                vertico-buffer
                                vertico-repeat
                                vertico-reverse
                                vertico-directory
                                vertico-multiform
                                vertico-unobtrusive
                                ))
    :custom
    (vertico-count 13)
    (vertico-resize t)
    (vertico-cycle t)
    :config
    (vertico-mode))

(use-package vertico-directory
  :after vertico
  :ensure nil
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(use-package orderless
  :init
  ;; Configure completion style
  (setq completion-styles '(orderless partial-completion basic)
        completion-category-defaults nil
        completion-category-overrides nil))

;; Annotations
(use-package marginalia
  :init
  (marginalia-mode))

;;
;; Completion at-point
;;

(use-package corfu
  :hook
  (eval-expression-minibuffer-setup . corfu-mode)
  :custom
  (corfu-cycle t)
  (corfu-auto t)
  :init
  (global-corfu-mode)
  (setq completion-cycle-threshold 3
        tab-always-indent 'complete))


(use-package svg-lib)
(use-package kind-icon
  :ensure t
  :after corfu
  :custom
  (kind-icon-use-icons t)
  (kind-icon-default-face 'corfu-default)
  (kind-icon-blend-background nil)
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

;;
;; History
;;

(use-package savehist
  :config
  (setq history-length 25)
  :init
  (savehist-mode))
