;;
;; Vim Emulation
;;

(use-package evil
  :init
  (setq evil-want-integration t
        evil-want-keybinding nil
        evil-want-C-u-scroll t
        evil-want-C-i-jump t
        evil-respect-visual-line-mode t
        evil-undo-system 'undo-fu)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; User visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  ;; Set initial states for modes
  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :ensure t
  :custom
  (evil-collection-outline-bind-tab-p nil)
  :config
  (evil-collection-init))

(define-key evil-normal-state-map "/" 'consult-line)

(use-package evil-nerd-commenter)

(use-package evil-surround
  :ensure t
  :config
  (global-evil-surround-mode 1))

(use-package evil-snipe
  :ensure t
  :config
  (evil-snipe-mode +1)
  (evil-snipe-override-mode +1))

;;
;; Key Prompts
;;

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-dile-delay 0.3))

;;
;; Leader Keys
;;

(use-package general
  :config
  (general-evil-setup t)
  (general-create-definer dw/leader-key-def
    :keymaps '(normal insert visual emacs)
    :prefix ","
    :global-prefix "M-,")

  (general-create-definer dw/crtl-c-keys
    :prefix "C-c"))

(dw/leader-key-def
  "e" '(:ignore t :which-key "eval")
  "eb" '(eval-buffer :which-key "eval buffer")
  "pf" 'consult-projectile-find-file
  "ps" 'consult-projectile-switch-project
  "pF" 'consult-ripgrep
  "pp" 'consult-projectile
  "pc" 'projectile-compile-project
  "pd" 'projectile-dired
  "cc" 'evilnc-comment-or-uncomment-lines
  "l" '(:ignore t :which-key "lsp")
  "ln" 'lsp-ui-find-next-reference
  "lp" 'lsp-ui-find-prev-reference
  "le" 'lsp-ui-flycheck-list
  "lS" 'lsp-ui-sideline-mode
  "lX" 'lsp-execute-code-action
  "lt" 'lsp-ui-imenu
  "tf" 'treemacs
  "o" '(:ignore t :whick-key "org mode")
  "oi" '(:ignore t :which-key "insert link")
  "oil" '(org-insert-link :which-key "insert link")
  "on" '(org-toggle-narrow-to-subtree :which-key "toggle narrow")
  "oa" '(org-agenda :which-key "status")
  "oc" '(org-capture t :which-key "capture")
  "ox" '(org-export-dispatch t :which-key "export")
  "vt" '(multi-vterm-project :which-key "vterm"))
