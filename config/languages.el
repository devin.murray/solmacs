;; Devops Languages
(use-package yaml-mode
  :ensure t
  :config
  (use-package flycheck-yamllint
    :ensure))

(use-package ansible
   :ensure t
   ;; :commands (ansible ansible-doc)
   :after yaml-mode
   :hook (yaml-mode . (lambda ()
                        ;; Enable ansible-doc-mode, or any ansible specific setup here.
                        (ansible-doc-mode 1)
                        (ansible 1)
                        (lsp 1)))
   :config
   (setq lsp-ansible-add-on? t)
   )
(use-package ansible-doc
  :after ansible)
(use-package hcl-mode)
(use-package terraform-mode)

;; Config Language
(use-package json-mode)
(use-package apache-mode)
(use-package nginx-mode)
(use-package dockerfile-mode)

;; Nix
(use-package nix-mode)

(use-package direnv
  :config
  (direnv-mode)
  (setq direnv-always-show-summary nil
        direnv-show-paths-in-summary nil))

;; Powershell
(use-package powershell
  :straight (powershell.el :type git :host github :repo "jschaf/powershell.el"))

;; Markdown
(use-package markdown-mode)
