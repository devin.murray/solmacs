(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/Projects")
    (setq projectile-project-search-path '("~/Projects")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package consult-projectile
  :after projectile)

(use-package magit
  :bind
  (:prefix-map magit-prefix-map
		  :prefix "C-c m"
		  (("a" . magit-stage-file) ; the closest analog to git add
		   ("b" . magit-blame)
		   ("B" . magit-branch)
		   ("c" . magit-checkout)
		   ("C" . magit-commit)
		   ("d" . magit-diff)
		   ("D" . magit-discard)
		   ("f" . magit-fetch)
		   ("g" . vc-git-grep)
		   ("G" . magit-gitignore)
		   ("i" . magit-init)
		   ("l" . magit-log)
		   ("m" . magit)
		   ("M" . magit-merge)
		   ("n" . magit-notes-edit)
		   ("p" . magit-pull)
		   ("P" . magit-push)
		   ("r" . magit-reset)
		   ("R" . magit-rebase)
		   ("s" . magit-status)
		   ("S" . magit-stash)
		   ("t" . magit-tag)
		   ("T" . magit-tag-delete)
		   ("u" . magit-unstage)
		   ("U" . magit-update-index))))

(use-package forge
  :after magit)

(use-package git-gutter
  :config
  (global-git-gutter-mode +1)
  (setq git-gutter:update-interval 0.02))

(use-package git-gutter-fringe
  :config
  (define-fringe-bitmap 'git-gutter-fr:added [224] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:modified [224] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:deleted [128 192 224 240] nil nil 'bottom))
