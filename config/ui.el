 ;; Line numbers
(column-number-mode)
(setq display-line-numbers-type 'visual)

;; Enable line numbers for certain modes
(dolist (mode '(text-mode-hook
                prog-mode-hook
                conf-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

;; Highlight delimiters
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;;
;; Sublime Features
;;

;; Smooth Scrolling
(use-package good-scroll
  :config
  (good-scroll-mode 1)
  (global-set-key [next] #'good-scroll-up)
  (global-set-key [prior] #'good-scroll-down)
  (setq good-scroll-step 240))
;;
;; File Explorer
(use-package treemacs
  :config
  (treemacs-project-follow-mode)
  (treemacs-git-mode 'deferred)
  (treemacs-filewatch-mode)
  (treemacs-indent-guide-mode 'line)
  (treemacs-git-commit-diff-mode))
(use-package treemacs-evil)
(use-package treemacs-projectile)

;;
;;
;; Theme
;;
(use-package all-the-icons)
(use-package all-the-icons-completion
  :config
  (all-the-icons-completion-mode))


(use-package doom-themes
  :config
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t)
  (load-theme 'doom-one t)
  (setq doom-themes-treemacs-theme "doom-colors")
  (doom-themes-treemacs-config)
  (doom-themes-visual-bell-config)
  (doom-themes-org-config))

;;
;; Syntax Highlighting
;;

(use-package tree-sitter
  :hook (
         ((sh-mode c-mode c++-mode c-sharp-mode dockerfile-mode gdscript-mode go-mode hcl-mode html-mode java-mode javascript-mode json-mode kotlin-mode lua-mode make-mode markdown-mode nix-mode org-mode php-mode python-mode ruby-mode rust-mode sql-mode terraform-mode toml-mode typescript-mode xml-mode yaml-mode) . tree-sitter-hl-mode))
  :config
  (push '(forge-post-mode . markdown) tree-sitter-major-mode-language-alist))
(use-package tree-sitter-langs)

;;
;; Fonts
;;

(setq default-frame-alist
      (append (list '(width . 72) '(height . 40)
                    '(vertical-scroll-bars . nil)
                    '(internal-border-width . 24))))

(set-frame-font "monospace 13" nil t)
(setq default-frame-alist '(( font . "monospace 13")))

(use-package unicode-fonts
  :custom
  (unicode-fonts-skip-font-groups '(low-quality-glyphs))
  :config
  (unicode-fonts-setup))

(set-frame-parameter (selected-frame)
                     'internal-border-width 24)
(setq frame-resize-pixelwise t)

;; Emoji support
(use-package emojify
  :hook (erc-mode . emojify-mode) (dashboard-mode . emojify-mode) (vterm-mode . emojify-mode)
  :commands emojify-mode)

;;
;; Modeline
;;

;; Hide dumb minor modes
(use-package diminish)

(use-package smart-mode-line
  :disabled
  :config
  (setq sml/no-confirm-load-theme t)
  (sml/setup)
  (sml/apply-theme 'respectful)
  (setq sml/mode-width 'right
        sml/name-width 60)
  (setq rm-excluded-modes
        (mapconcat
         'identity
         '(" company" " Org-Agenda.*" " ElDoc")
         "\\|")))

(use-package minions
  :hook (doom-modeline-mode . minions-mode)
  :custom
  (minions-mode-line-lighter ""))


;; Doom modeline
(use-package doom-modeline
  :defer t
  :hook (after-init . doom-modeline-mode)
  :config
  (setq doom-modeline-bar-width 3
        doom-modeline-height 35
        doom-modeline-github nil
        doom-modeline-irc nil
        doom-modeline-mu4e nil
        doom-modeline-icon t
        doom-modeline-buffer-file-name-style 'truncate-except-project
        doom-modeline-enable-word-count t
        doom-modeline-minor-modes t
        doom-modeline-persp-name nil
        doom-modeline-major-mode-icon t))
   (setq doom-modeline-continuous-word-count-modes '(markdown-mode gfm-mode org-mode))


(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-startup-banner "~/.emacs.d/GabEmacs.png")
  (setq dashboard-banner-logo-title "Happy Hacking~ ❤")
  (setq dashboard-center-content t)
  (setq dashboard-show-shortcuts nil)

  (setq dashboard-items '((recents . 5)
                          (agenda . 5)))
  (setq dashboard-week-agenda t)

  (setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*"))))

(use-package vertico-posframe
  :straight (vertico-posframe :type git :host github :repo "tumashu/vertico-posframe")
  :config
  (vertico-posframe-mode 1))
