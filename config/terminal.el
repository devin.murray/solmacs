(use-package vterm)

(use-package multi-vterm
  :config
  (setq multi-vterm-dedicated-window-height-percent 30))
